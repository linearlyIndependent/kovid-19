/*
 * *
 *  * [Kovid-19] MaskedCardView.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 6/30/20 8:32 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 6/30/20 8:32 PM.
 *
 */

package xyz.ploechl.kovid_19.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Path
import android.graphics.RectF
import android.util.AttributeSet
import com.google.android.material.card.MaterialCardView
import com.google.android.material.shape.ShapeAppearanceModel
import com.google.android.material.shape.ShapeAppearancePathProvider
import com.google.android.material.R

class MaskedCardView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = R.attr.materialCardViewStyle
): MaterialCardView(context, attrs, defStyle){
    @SuppressLint("RestrictedApi")
    private val pathProvider = ShapeAppearancePathProvider()
    private val path: Path = Path()
    private val rectF = RectF(0f,0f,0f,0f)

    //TODO: check this out now fixed with builder
    private val shapeAppearance: ShapeAppearanceModel = ShapeAppearanceModel.builder(context, attrs, defStyle, R.style.Widget_MaterialComponents_CardView).build()

    override fun onDraw(canvas: Canvas) {
        canvas.clipPath(this.path)
        super.onDraw(canvas)
    }

    @SuppressLint("RestrictedAPI")
    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        rectF.right = w.toFloat()
        rectF.bottom = h.toFloat()
        pathProvider.calculatePath(this.shapeAppearance, 1f, this.rectF, this.path)
        super.onSizeChanged(w, h, oldw, oldh)
    }
}