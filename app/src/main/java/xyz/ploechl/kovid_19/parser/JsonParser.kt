/*
 * *
 *  * [Kovid-19] JsonParser.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 5/19/20 9:55 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 5/19/20 9:55 PM.
 *
 */

package xyz.ploechl.kovid_19.parser

import android.content.Context
import android.content.res.AssetManager
import android.util.JsonReader
import android.util.Log
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import xyz.ploechl.kovid_19.data.CovidData
import xyz.ploechl.kovid_19.data.Setting
import xyz.ploechl.kovid_19.utility.*
import xyz.ploechl.kovid_19.utility.L0_DATA
import xyz.ploechl.kovid_19.utility.L0_DT
import xyz.ploechl.kovid_19.utility.L0_TS
import xyz.ploechl.kovid_19.utility.L1_ACTIVE
import xyz.ploechl.kovid_19.utility.L1_LOCATION
import java.nio.file.AccessMode
import java.text.SimpleDateFormat
import java.util.*
import java.text.ParseException

/**
 * Parses a JSON string and retrieves the country covid stats into [CovidData] objects and returns them in a list.
 */
internal fun parseCovidCountryDataFromJsonString(jsonString: String): List<CovidData>? {
    val parsedJson: JSONObject

    try {
        parsedJson = JSONObject(jsonString)

    } catch (e: JSONException) {
        Log.e(::parseCovidCountryDataFromJsonString.toString(), "Error while parsing JSON object!", e)
        return null
    }

    val dt: Date
    val ts: Int

    try {
        ts = parsedJson.getInt(L0_TS)
        dt = DATE_FORMATTER.parse(parsedJson.getString(L0_DT)) ?: throw ParseException("dt parsing returned null!", 0)
    } catch (e: JSONException) {
        Log.e(::parseCovidCountryDataFromJsonString.toString(), "Error while parsing JSON dt and ts!", e)
        return null
    } catch (e: ParseException) {
        Log.e(::parseCovidCountryDataFromJsonString.toString(), "Error while parsing JSON dt and ts!", e)
        return null
    }

    val dataArray: JSONArray

    try {
        dataArray = parsedJson.getJSONArray(L0_DATA)

    } catch (e: JSONException) {
        Log.e(::parseCovidCountryDataFromJsonString.toString(), "Error while parsing JSON array!", e)
        return null
    }

    Log.d(::parseCovidCountryDataFromJsonString.toString(), "Retrieved JSON with ${dataArray.length()} entries.")

    val covidDataList = mutableListOf<CovidData>()

    for (i in 0 until dataArray.length()) {
        try {
            val covidDataJson = dataArray.getJSONObject(i)
            val name = covidDataJson.getString(L1_LOCATION)
            val active = covidDataJson.getInt(L1_ACTIVE)
            val confirmed = covidDataJson.getInt(L1_CONFIRMED)
            val deaths = covidDataJson.getInt(L1_DEATHS)
            val recovered = covidDataJson.getInt(L1_RECOVERED)

            covidDataList.add(CovidData(name, confirmed, active, deaths, recovered, true, dt, ts))

        } catch (e: JSONException) {
            Log.e(::parseCovidCountryDataFromJsonString.toString(), "Error while parsing JSON array entry at index ${i}!", e)
            return null
        }
    }

    Log.d(::parseCovidCountryDataFromJsonString.toString(), "Sucessfully parsed ${covidDataList.count()}/${dataArray.length()} entries.")

    return covidDataList
}


/**
 * Parses a JSON string into JSON objects and retrieves the global covid stat as a [CovidData] object.
 */
internal fun parseCovidWorldDataFromJsonString(jsonString: String): CovidData? {
    val parsedJson: JSONObject

    try {
        parsedJson = JSONObject(jsonString)

    } catch (e: JSONException) {
        Log.e(::parseCovidWorldDataFromJsonString.toString(), "Error while parsing JSON object!", e)
        return null
    }

    val dt: Date
    val ts: Int

    try {
        ts = parsedJson.getInt(L0_TS)
        dt = DATE_FORMATTER.parse(parsedJson.getString(L0_DT)) ?: throw ParseException("dt parsing returned null!", 0)
    } catch (e: JSONException) {
        Log.e(::parseCovidWorldDataFromJsonString.toString(), "Error while parsing JSON dt and ts!", e)
        return null
    } catch (e: ParseException) {
        Log.e(::parseCovidWorldDataFromJsonString.toString(), "Error while parsing JSON dt and ts!", e)
        return null
    }

    val jsonObject: JSONObject

    try {
        jsonObject = parsedJson.getJSONObject(L0_DATA)

    } catch (e: JSONException) {
        Log.e(::parseCovidWorldDataFromJsonString.toString(), "Error while extracting JSON data object!", e)
        return null
    }

    val covidData: CovidData

    try {
        val active = jsonObject.getInt(L1_ACTIVE)
        val confirmed = jsonObject.getInt(L1_CONFIRMED)
        val deaths = jsonObject.getInt(L1_DEATHS)
        val recovered = jsonObject.getInt(L1_RECOVERED)

        covidData = CovidData(CovidData.WORLD_ID, confirmed, active, deaths, recovered, false, dt, ts)

    } catch (e: JSONException) {
        Log.e(::parseCovidWorldDataFromJsonString.toString(), "Error while parsing JSON data object!", e)
        return null
    }

    return covidData
}