/*
 * *
 *  * [Kovid-19] SettingRepository.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 7/5/20 9:48 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 7/5/20 9:48 PM.
 *
 */

package xyz.ploechl.kovid_19.data

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class SettingRepository private constructor(private val settingDao: SettingDao) {

    suspend fun saveSettingEntry(key: String, value: String) = withContext(Dispatchers.IO) { settingDao.insertSettingValue(Setting(key, value)) }

    //TODO: delete these if not needed
    suspend fun saveSettingEntry(setting: Setting) = this.settingDao.insertSettingValue(setting)

    suspend fun SaveSettings(settingsList: List<Setting>) = this.settingDao.insertSettingsList(settingsList)

    fun getSettings() = this.settingDao.getSettings()

    companion object {
        @Volatile private var instance: SettingRepository? = null

        fun getInstance(settingDao: SettingDao) =
            this.instance ?: synchronized(this) {
                this.instance ?: SettingRepository(settingDao).also {
                    this.instance = it
                }
            }
    }
}