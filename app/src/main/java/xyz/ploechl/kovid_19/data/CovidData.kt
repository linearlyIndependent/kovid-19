/*
 * *
 *  * [Kovid-19] CovidData.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 6/26/20 6:48 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 6/26/20 6:48 PM.
 *
 */

package xyz.ploechl.kovid_19.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

/**
 * Represents one Covid data entry.
 */
@Entity(tableName="covid_data")
data class CovidData(
    @PrimaryKey @ColumnInfo(name="id") val name: String,
    val confirmed: Int,
    val active: Int,
    val deaths: Int,
    val recovered: Int,
    val isCountry: Boolean,
    val dt: Date,
    val ts: Int
) {
    override fun toString() = "${this.name}: ${this.confirmed} ${this.active} ${this.recovered} ${this.deaths}"

    companion object {
        /**
         * ID of the covid data that holds the total global numbers.
         */
        const val WORLD_ID = "world"
    }
}