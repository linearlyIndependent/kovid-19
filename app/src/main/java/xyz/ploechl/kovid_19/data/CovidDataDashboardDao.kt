/*
 * *
 *  * [Kovid-19] CovidDataDashboardDao.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 6/26/20 8:09 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 6/26/20 8:09 PM.
 *
 */

package xyz.ploechl.kovid_19.data

import androidx.lifecycle.LiveData
import androidx.room.*

/**
 * The Data Access Object for the [CovidDataDashboard] class.
 */
@Dao
interface CovidDataDashboardDao {
    /**
     * Returns the [CovidDataDashboard]s from the database.
     */
    @Query("SELECT * FROM covid_data_dashboard")
    fun getCovidDataDashboardDataset(): LiveData<List<CovidDataDashboard>>

    /**
     * Returns whether a [CovidData] entry is on the [CovidDataDashboard].
     */
    @Query("SELECT EXISTS(SELECT 1 FROM covid_data_dashboard WHERE covid_data_id = :covidDataId LIMIT 1)")
    fun isOnDashboard(covidDataId: String): LiveData<Boolean>

    /**
     * Returns the [CovidDataDashboard] containing the [CovidData] based on the ID.
     */
     @Transaction
     @Query("SELECT * FROM covid_data WHERE id IN (SELECT DISTINCT(covid_data_id) FROM covid_data_dashboard) AND id=:covidDataId")
     fun getDashboardsOfCovidDataSync(covidDataId: String): LiveData<List<CovidDataAndCovidDataDashboards>>

    /**
     * Returns for all [CovidData]s entries all the [CovidDataDashboard]s they are contained in, ascending name.
     */
    @Transaction
    @Query("SELECT * FROM covid_data WHERE id IN (SELECT DISTINCT(covid_data_id) FROM covid_data_dashboard) ORDER BY id ASC")
    fun getCreatedCovidDataDashboardsNameAsc(): LiveData<List<CovidDataAndCovidDataDashboards>>

    /**
     * Returns for all [CovidData]s entries all the [CovidDataDashboard]s they are contained in, descending name.
     */
    @Transaction
    @Query("SELECT * FROM covid_data WHERE id IN (SELECT DISTINCT(covid_data_id) FROM covid_data_dashboard) ORDER BY id DESC")
    fun getCreatedCovidDataDashboardsNameDesc(): LiveData<List<CovidDataAndCovidDataDashboards>>

    /**
     * Returns for all [CovidData]s entries all the [CovidDataDashboard]s they are contained in, ascending active.
     */
    @Transaction
    @Query("SELECT * FROM covid_data WHERE id IN (SELECT DISTINCT(covid_data_id) FROM covid_data_dashboard) ORDER BY active ASC")
    fun getCreatedCovidDataDashboardsActiveAsc(): LiveData<List<CovidDataAndCovidDataDashboards>>

    /**
     * Returns for all [CovidData]s entries all the [CovidDataDashboard]s they are contained in, descending active.
     */
    @Transaction
    @Query("SELECT * FROM covid_data WHERE id IN (SELECT DISTINCT(covid_data_id) FROM covid_data_dashboard) ORDER BY active DESC")
    fun getCreatedCovidDataDashboardsActiveDesc(): LiveData<List<CovidDataAndCovidDataDashboards>>

    /**
     * Returns for all [CovidData]s entries all the [CovidDataDashboard]s they are contained in, ascending confirmed.
     */
    @Transaction
    @Query("SELECT * FROM covid_data WHERE id IN (SELECT DISTINCT(covid_data_id) FROM covid_data_dashboard) ORDER BY confirmed ASC")
    fun getCreatedCovidDataDashboardsConfirmedAsc(): LiveData<List<CovidDataAndCovidDataDashboards>>

    /**
     * Returns for all [CovidData]s entries all the [CovidDataDashboard]s they are contained in, descending confirmed.
     */
    @Transaction
    @Query("SELECT * FROM covid_data WHERE id IN (SELECT DISTINCT(covid_data_id) FROM covid_data_dashboard) ORDER BY confirmed DESC")
    fun getCreatedCovidDataDashboardsConfirmedDesc(): LiveData<List<CovidDataAndCovidDataDashboards>>

    /**
     * Returns for all [CovidData]s entries all the [CovidDataDashboard]s they are contained in, ascending recovered.
     */
    @Transaction
    @Query("SELECT * FROM covid_data WHERE id IN (SELECT DISTINCT(covid_data_id) FROM covid_data_dashboard) ORDER BY recovered ASC")
    fun getCreatedCovidDataDashboardsRecoveredAsc(): LiveData<List<CovidDataAndCovidDataDashboards>>

    /**
     * Returns for all [CovidData]s entries all the [CovidDataDashboard]s they are contained in, descending recovered.
     */
    @Transaction
    @Query("SELECT * FROM covid_data WHERE id IN (SELECT DISTINCT(covid_data_id) FROM covid_data_dashboard) ORDER BY recovered DESC")
    fun getCreatedCovidDataDashboardsRecoveredDesc(): LiveData<List<CovidDataAndCovidDataDashboards>>

    /**
     * Returns for all [CovidData]s entries all the [CovidDataDashboard]s they are contained in, ascending deaths.
     */
    @Transaction
    @Query("SELECT * FROM covid_data WHERE id IN (SELECT DISTINCT(covid_data_id) FROM covid_data_dashboard) ORDER BY deaths ASC")
    fun getCreatedCovidDataDashboardsDeathsAsc(): LiveData<List<CovidDataAndCovidDataDashboards>>

    /**
     * Returns for all [CovidData]s entries all the [CovidDataDashboard]s they are contained in, descending deaths.
     */
    @Transaction
    @Query("SELECT * FROM covid_data WHERE id IN (SELECT DISTINCT(covid_data_id) FROM covid_data_dashboard) ORDER BY deaths DESC")
    fun getCreatedCovidDataDashboardsDeathsDesc(): LiveData<List<CovidDataAndCovidDataDashboards>>

    /**
     * Places a [CovidDataDashboard] into the database.
     */
    @Insert
    suspend fun insertCovidDataDashboard(covidDataDashboard: CovidDataDashboard): Long

    /**
     * Deletes all [CovidDataDashboard]s containing a covidData.
     */
    @Query("DELETE FROM covid_data_dashboard WHERE covid_data_id=:covidDataId")
    suspend fun deleteCovidDataDashboards(covidDataId: String)
}