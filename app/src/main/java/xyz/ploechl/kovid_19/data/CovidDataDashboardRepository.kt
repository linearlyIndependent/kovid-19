/*
 * *
 *  * [Kovid-19] CovidDataDashboardRepository.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 6/26/20 9:50 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 6/26/20 9:50 PM.
 *
 */

package xyz.ploechl.kovid_19.data

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import xyz.ploechl.kovid_19.worker.Kovid19DatabaseWorker
import java.util.Date

/**
 * The repository object to manage [CovidDataDashboard]s through [CovidDataDashboardDao]s.
 */
class CovidDataDashboardRepository private constructor(private val covidDataDashboardDao: CovidDataDashboardDao) {
    /**
     * Creates and saves a new [CovidDataDashboard].
     */
    suspend fun createCovidDataDashboard(covidDataId: String) = this.covidDataDashboardDao.insertCovidDataDashboard(CovidDataDashboard(covidDataId, Date()))

    /**
     * Removes a [CovidDataDashboard] from the database.
     */
    suspend fun deleteCovidDataDashboards(covidDataId: String) = this.covidDataDashboardDao.deleteCovidDataDashboards(covidDataId)

    /**
     * Returns if a [CovidData] entry is on any dashboard.
     */
    fun isCovidDataOnDashboard(covidDataId: String) = this.covidDataDashboardDao.isOnDashboard(covidDataId)

    /**
     * Returns the created [CovidDataDashboard]s as [CovidDataAndCovidDataDashboards].
     */
    fun getCreatedCovidDataDashboards(ordering: Setting.Companion.OrderingValue, ascDesc: Setting.Companion.AscDescValue): LiveData<List<CovidDataAndCovidDataDashboards>> {
        var data: LiveData<List<CovidDataAndCovidDataDashboards>>? = null

        // I simply love Kotlin for things like this.
        try {
            data = when (ordering) {
                Setting.Companion.OrderingValue.NAME -> when (ascDesc) {
                    Setting.Companion.AscDescValue.ASCENDING -> covidDataDashboardDao.getCreatedCovidDataDashboardsNameAsc()
                    Setting.Companion.AscDescValue.DESCENDING -> covidDataDashboardDao.getCreatedCovidDataDashboardsNameDesc()
                }
                Setting.Companion.OrderingValue.CONFIRMED -> when (ascDesc) {
                    Setting.Companion.AscDescValue.ASCENDING -> covidDataDashboardDao.getCreatedCovidDataDashboardsConfirmedAsc()
                    Setting.Companion.AscDescValue.DESCENDING -> covidDataDashboardDao.getCreatedCovidDataDashboardsConfirmedDesc()
                }
                Setting.Companion.OrderingValue.ACTIVE -> when (ascDesc) {
                    Setting.Companion.AscDescValue.ASCENDING -> covidDataDashboardDao.getCreatedCovidDataDashboardsActiveAsc()
                    Setting.Companion.AscDescValue.DESCENDING -> covidDataDashboardDao.getCreatedCovidDataDashboardsActiveDesc()
                }
                Setting.Companion.OrderingValue.RECOVERED -> when (ascDesc) {
                    Setting.Companion.AscDescValue.ASCENDING -> covidDataDashboardDao.getCreatedCovidDataDashboardsRecoveredAsc()
                    Setting.Companion.AscDescValue.DESCENDING -> covidDataDashboardDao.getCreatedCovidDataDashboardsRecoveredDesc()
                }
                Setting.Companion.OrderingValue.DEATHS -> when (ascDesc) {
                    Setting.Companion.AscDescValue.ASCENDING -> covidDataDashboardDao.getCreatedCovidDataDashboardsDeathsAsc()
                    Setting.Companion.AscDescValue.DESCENDING -> covidDataDashboardDao.getCreatedCovidDataDashboardsDeathsDesc()
                }
            }

        } catch (ex: Exception) {
            Log.e(::getCreatedCovidDataDashboards.toString(), "Unexpected setting value.", ex)
        }

        //Catch-all
        if (data == null) {
            data = covidDataDashboardDao.getCreatedCovidDataDashboardsNameAsc()
        }

        return data
    }

    /**
     * Updates the data from the remote API.
     */
    fun updateCovidDataFromRemote() {
        WorkManager.getInstance().enqueue(OneTimeWorkRequestBuilder<Kovid19DatabaseWorker>().build())
    }

    companion object {
        @Volatile private var instance: CovidDataDashboardRepository? = null

        fun getInstance(covidDataDashboardDao: CovidDataDashboardDao) =
            this.instance ?: synchronized(this) {
                this.instance ?: CovidDataDashboardRepository(covidDataDashboardDao).also {
                    this.instance = it
                }
            }
    }
}