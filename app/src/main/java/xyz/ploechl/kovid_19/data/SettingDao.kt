/*
 * *
 *  * [Kovid-19] SettingDao.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 7/5/20 9:40 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 7/5/20 9:40 PM.
 *
 */

package xyz.ploechl.kovid_19.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface SettingDao {
    @Query("SELECT * FROM setting WHERE id = :key")
    fun getSettingValue(key: String): LiveData<Setting>

    @Query("SELECT * FROM setting")
    fun getSettings(): LiveData<List<Setting>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSettingValue(setting: Setting)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSettingsList(settingsList: List<Setting>)
}