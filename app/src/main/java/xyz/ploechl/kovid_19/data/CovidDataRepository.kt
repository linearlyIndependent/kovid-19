/*
 * *
 *  * [Kovid-19] CovidDataRepository.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 6/26/20 10:45 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 6/26/20 10:45 PM.
 *
 */

package xyz.ploechl.kovid_19.data

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import xyz.ploechl.kovid_19.worker.Kovid19DatabaseWorker
import java.lang.Exception

class CovidDataRepository private constructor(private val covidDataDao: CovidDataDao) {

    /**
     * Returns all [CovidData] entries with the right ordering.
     */
    fun getCovidDataEntries(ordering: Setting.Companion.OrderingValue, ascDesc: Setting.Companion.AscDescValue): LiveData<List<CovidData>> {
        var data: LiveData<List<CovidData>>? = null

        // I simply love Kotlin for things like this.
        try {
            data = when (ordering) {
                Setting.Companion.OrderingValue.NAME -> when (ascDesc) {
                    Setting.Companion.AscDescValue.ASCENDING -> covidDataDao.getCovidDataNameAsc()
                    Setting.Companion.AscDescValue.DESCENDING -> covidDataDao.getCovidDataNameDesc()
                }
                Setting.Companion.OrderingValue.CONFIRMED -> when (ascDesc) {
                    Setting.Companion.AscDescValue.ASCENDING -> covidDataDao.getCovidDataConfirmedAsc()
                    Setting.Companion.AscDescValue.DESCENDING -> covidDataDao.getCovidDataConfirmedDesc()
                }
                Setting.Companion.OrderingValue.ACTIVE -> when (ascDesc) {
                    Setting.Companion.AscDescValue.ASCENDING -> covidDataDao.getCovidDataActiveAsc()
                    Setting.Companion.AscDescValue.DESCENDING -> covidDataDao.getCovidDataActiveDesc()
                }
                Setting.Companion.OrderingValue.RECOVERED -> when (ascDesc) {
                    Setting.Companion.AscDescValue.ASCENDING -> covidDataDao.getCovidDataRecoveredAsc()
                    Setting.Companion.AscDescValue.DESCENDING -> covidDataDao.getCovidDataRecoveredDesc()
                }
                Setting.Companion.OrderingValue.DEATHS -> when (ascDesc) {
                    Setting.Companion.AscDescValue.ASCENDING -> covidDataDao.getCovidDataDeathsAsc()
                    Setting.Companion.AscDescValue.DESCENDING -> covidDataDao.getCovidDataDeathsDesc()
                }
            }

        } catch (ex: Exception) {
            Log.e(::getCovidDataEntries.toString(), "Unexpected setting value.", ex)
        }

        //Catch-all
        if (data == null) {
            data = covidDataDao.getCovidDataNameAsc()
        }

        return data
    }

    /**
     * Returns a single [CovidData] entry based on its ID.
     */
    fun getCovidData(covidDataId: String) = this.covidDataDao.getSingleCovidData(covidDataId)

    /**
     * Updates the data from the remote API.
     */
    suspend fun updateCovidDataFromRemote() = withContext(Dispatchers.IO) {
        WorkManager.getInstance().enqueue(OneTimeWorkRequestBuilder<Kovid19DatabaseWorker>().build())
        //TODO: if this crashes it might be because of getInstance
    }

    companion object {
        @Volatile private var instance: CovidDataRepository? = null

        fun getInstance(covidDataDao: CovidDataDao) =
            this.instance ?: synchronized(this) {
                this.instance ?: CovidDataRepository(covidDataDao).also {
                    this.instance = it
                }
            }
    }
}