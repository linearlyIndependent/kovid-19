/*
 * *
 *  * [Kovid-19] Converters.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 6/27/20 5:32 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 6/27/20 5:32 PM.
 *
 */

package xyz.ploechl.kovid_19.data

import androidx.room.TypeConverter
import java.util.*

/**
 * Converters to enable Room to persist more complex datatypes.
 */
class Converters {

    @TypeConverter
    fun dateToLong(date: Date): Long = date.time

    @TypeConverter
    fun longToDate(long: Long): Date = Date(long)
}