/*
 * *
 *  * [Kovid-19] CovidDataDashboard.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 6/26/20 7:25 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 6/26/20 7:25 PM.
 *
 */

package xyz.ploechl.kovid_19.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import java.util.*

/**
 * Represents when a geographic region [CovidData] is added to the dashboard [CovidDataDashboard].
 */
@Entity(
    tableName = "covid_data_dashboard",
    foreignKeys = [
        ForeignKey(entity = CovidData::class, parentColumns = ["id"], childColumns = ["covid_data_id"])
    ],
    indices = [Index("covid_data_id")]
)
class CovidDataDashboard(
    @ColumnInfo(name = "covid_data_id") val covidDataId: String,
    @ColumnInfo(name = "added_date") val addedDate: Date
) {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var covidDataDashboardId: Long = 0

    override fun toString() = "${this.covidDataDashboardId}: ${this.covidDataId} ${this.addedDate}"
}