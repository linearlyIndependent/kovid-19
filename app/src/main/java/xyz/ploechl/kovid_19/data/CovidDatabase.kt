/*
 * *
 *  * [Kovid-19] CovidDatabase.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 6/26/20 9:41 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 6/26/20 9:41 PM.
 *
 */

package xyz.ploechl.kovid_19.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import xyz.ploechl.kovid_19.utility.DATABASE_NAME
import xyz.ploechl.kovid_19.worker.Kovid19DatabaseWorker
import xyz.ploechl.kovid_19.worker.SettingDatabaseWorker

/**
 * The Room database which stores all the data of the app.
 */
@Database(entities = [CovidData::class, CovidDataDashboard::class, Setting::class], version = 2, exportSchema = false)
@TypeConverters(Converters::class)
abstract class CovidDatabase: RoomDatabase() {
    abstract fun covidDataDao(): CovidDataDao
    abstract fun covidDataDashboardDao(): CovidDataDashboardDao
    abstract fun settingDao(): SettingDao

    companion object {
        @Volatile private var instance: CovidDatabase? = null

        fun getInstance(context: Context): CovidDatabase =
            this.instance ?: synchronized(this) {
                this.instance ?: buildDatabase(context).also {
                    this.instance = it
                }
            }

        /**
         * Creates the initial database.
         */
        private fun buildDatabase(context: Context): CovidDatabase =
            Room.databaseBuilder(context, CovidDatabase::class.java, DATABASE_NAME)
                .addCallback(object: RoomDatabase.Callback(){
                    override fun onCreate(db: SupportSQLiteDatabase) {
                        super.onCreate(db)
                        WorkManager.getInstance(context).enqueue(OneTimeWorkRequestBuilder<SettingDatabaseWorker>().build())
                        WorkManager.getInstance(context).enqueue(OneTimeWorkRequestBuilder<Kovid19DatabaseWorker>().build())
                    }
                }).build()
    }
}