/*
 * *
 *  * [Kovid-19] Setting.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 7/5/20 9:31 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 7/5/20 9:31 PM.
 *
 */

package xyz.ploechl.kovid_19.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName="setting")
data class Setting(
    @PrimaryKey @ColumnInfo(name = "id") val key: String,
    val value: String
) {
    override fun toString(): String =
        "key: ${this.key}, value: ${this.value}"

    companion object {
        const val ORDERING_KEY = "ordering"
        const val ASC_DESC_KEY = "asc_desc"

        enum class OrderingValue {
            NAME, CONFIRMED, ACTIVE, RECOVERED, DEATHS
        }

        enum class AscDescValue {
            ASCENDING, DESCENDING
        }
    }
}

/*
/**
 * Represents one Covid data entry.
 */
@Entity(tableName="covid_data")
data class CovidData(
    @PrimaryKey @ColumnInfo(name="id") val name: String,
    val confirmed: Int,
    val active: Int,
    val deaths: Int,
    val recovered: Int,
    val isCountry: Boolean,
    val dt: Date,
    val ts: Int
) {
    override fun toString() = "${this.name}: ${this.confirmed} ${this.active} ${this.recovered} ${this.deaths}"

    companion object {
        const val WORLD_ID = "world"

        enum class CovidDataOrdering {
            NAME_ASC, NAME_DESC, CONFIRMED_ASC, CONFIRMED_DESC, ACTIVE_ASC, ACTIVE_DESC, RECOVERED_ASC, RECOVERED_DESC, DEATHS_ASC, DEATHS_DESC
            //NAME_ASC(0), NAME_DESC(1), CONFIRMED_ASC(2), CONFIRMED_DESC(3), ACTIVE_ASC(4), ACTIVE_DESC(5), RECOVERED_ASC(6), RECOVERED_DESC(7), DEATHS_ASC(8), DEATHS_DESC(9)
        }
    }
}
* */