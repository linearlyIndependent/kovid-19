/*
 * *
 *  * [Kovid-19] CovidDataAndCovidDataDashboards.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 6/26/20 10:30 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 6/26/20 10:30 PM.
 *
 */

package xyz.ploechl.kovid_19.data

import androidx.room.Embedded
import androidx.room.Relation

/**
 * Provides the relation between a [CovidDataDashboard] and a[CovidData] entry.
 */
data class CovidDataAndCovidDataDashboards(
    @Embedded
    val covidData: CovidData,

    @Relation(parentColumn = "id", entityColumn = "covid_data_id")
    val covidDataDashboards: List<CovidDataDashboard> = emptyList()
)
