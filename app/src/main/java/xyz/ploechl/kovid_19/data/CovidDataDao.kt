/*
 * *
 *  * [Kovid-19] CovidDataDao.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 6/26/20 7:41 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 6/26/20 7:41 PM.
 *
 */

package xyz.ploechl.kovid_19.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

/**
 * The Data Access Object for the [CovidData] class.
 */
@Dao
interface CovidDataDao {


    /**
     * Returns all [CovidData] entries ascending name.
     */
    @Query("SELECT * FROM covid_data ORDER BY id ASC")
    fun getCovidDataNameAsc(): LiveData<List<CovidData>>


    /**
     * Returns all [CovidData] entries descending name.
     */
    @Query("SELECT * FROM covid_data ORDER BY id DESC")
    fun getCovidDataNameDesc(): LiveData<List<CovidData>>


    /**
     * Returns all [CovidData] entries ascending confirmed.
     */
    @Query("SELECT * FROM covid_data ORDER BY confirmed ASC")
    fun getCovidDataConfirmedAsc(): LiveData<List<CovidData>>


    /**
     * Returns all [CovidData] entries descending confirmed.
     */
    @Query("SELECT * FROM covid_data ORDER BY confirmed DESC")
    fun getCovidDataConfirmedDesc(): LiveData<List<CovidData>>


    /**
     * Returns all [CovidData] entries ascending active.
     */
    @Query("SELECT * FROM covid_data ORDER BY active ASC")
    fun getCovidDataActiveAsc(): LiveData<List<CovidData>>


    /**
     * Returns all [CovidData] entries descending active.
     */
    @Query("SELECT * FROM covid_data ORDER BY active DESC")
    fun getCovidDataActiveDesc(): LiveData<List<CovidData>>


    /**
     * Returns all [CovidData] entries ascending recovered.
     */
    @Query("SELECT * FROM covid_data ORDER BY recovered ASC")
    fun getCovidDataRecoveredAsc(): LiveData<List<CovidData>>


    /**
     * Returns all [CovidData] entries descending recovered.
     */
    @Query("SELECT * FROM covid_data ORDER BY recovered DESC")
    fun getCovidDataRecoveredDesc(): LiveData<List<CovidData>>


    /**
     * Returns all [CovidData] entries ascending deaths.
     */
    @Query("SELECT * FROM covid_data ORDER BY deaths ASC")
    fun getCovidDataDeathsAsc(): LiveData<List<CovidData>>


    /**
     * Returns all [CovidData] entries descending deaths.
     */
    @Query("SELECT * FROM covid_data ORDER BY deaths DESC")
    fun getCovidDataDeathsDesc(): LiveData<List<CovidData>>

    /**
     * Returns all [CovidData] country entries.
     */
    //@Query("SELECT * FROM covid_data WHERE isCountry ORDER BY id")
    //fun getCovidCountriesData(): LiveData<List<CovidData>>

    /**
     * Returns the world (global) covid data entry.
     */
    //@Query("SELECT * FROM covid_data WHERE NOT isCountry")
    //fun getCovidWorldData(): LiveData<List<CovidData>>

    /**
     * Returns a single [CovidData] entry based on its ID.
     */
    @Query("SELECT * FROM covid_data WHERE id = :covidDataId")
    fun getSingleCovidData(covidDataId: String): LiveData<CovidData>


    /**
     * Inserts [CovidData] entries into the table.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(covidDataList: List<CovidData>)
}