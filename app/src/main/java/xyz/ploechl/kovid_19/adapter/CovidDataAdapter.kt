/*
 * *
 *  * [Kovid-19] CovidDataAdapter.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 6/27/20 10:24 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 6/27/20 10:24 PM.
 *
 */

package xyz.ploechl.kovid_19.adapter

import android.app.LauncherActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavDirections
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import xyz.ploechl.kovid_19.HomeViewPagerFragment
import xyz.ploechl.kovid_19.HomeViewPagerFragmentDirections
import xyz.ploechl.kovid_19.data.CovidData
import xyz.ploechl.kovid_19.databinding.ListItemCovidDataBinding

/**
 * [RecyclerView] adapter in the [CovidDataListFragment].
 */
class CovidDataAdapter: ListAdapter<CovidData, RecyclerView.ViewHolder>(CovidDataDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        CovidDataViewHolder(ListItemCovidDataBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    /**
     * Get a [CovidData] and bind it to the right position in the viewholder.
     */
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as CovidDataViewHolder).bind(getItem(position))
    }

    class CovidDataViewHolder(private val binding: ListItemCovidDataBinding): RecyclerView.ViewHolder(binding.root) {
        init {
            binding.setClickListener {
                binding.covidData?.let { covidData ->
                    navigateToCovidData(covidData, it)
                }
            }
        }

        private fun navigateToCovidData(covidData: CovidData, view: View) {
            val direction: NavDirections = HomeViewPagerFragmentDirections.actionViewPagerFragmentToCovidDataDetailFragment(covidData.name)
            view.findNavController().navigate(direction)
        }

        fun bind(item: CovidData) =
            binding.apply {
                this.covidData = item
                executePendingBindings()
            }
    }
}

private class CovidDataDiffCallback: DiffUtil.ItemCallback<CovidData>() {

    override fun areItemsTheSame(oldItem: CovidData, newItem: CovidData): Boolean =
        oldItem.name == newItem.name

    override fun areContentsTheSame(oldItem: CovidData, newItem: CovidData): Boolean =
        oldItem == newItem
}