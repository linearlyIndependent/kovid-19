/*
 * *
 *  * [Kovid-19] CovidDataDashboardAdapter.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 6/27/20 7:26 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 6/27/20 7:26 PM.
 *
 */

package xyz.ploechl.kovid_19.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import xyz.ploechl.kovid_19.HomeViewPagerFragmentDirections
import xyz.ploechl.kovid_19.R
import xyz.ploechl.kovid_19.data.CovidDataAndCovidDataDashboards
import xyz.ploechl.kovid_19.databinding.ListItemCovidDataDashboardBinding
import xyz.ploechl.kovid_19.viewmodel.CovidDataAndCovidDataDashboardsViewModel

class CovidDataDashboardAdapter: ListAdapter<CovidDataAndCovidDataDashboards, CovidDataDashboardAdapter.ViewHolder>(CovidDataDashboardDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.list_item_covid_data_dashboard, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(this.getItem(position))

    class ViewHolder(private val binding: ListItemCovidDataDashboardBinding): RecyclerView.ViewHolder(binding.root) {
        init {
            binding.setClickListener { view ->
                binding.viewModel?.covidDataName?.let { covidDataId ->
                    navigateToCovidData(covidDataId, view)
                }

            }
        }

        private fun navigateToCovidData(name: String, view: View) {
            val direction = HomeViewPagerFragmentDirections.actionViewPagerFragmentToCovidDataDetailFragment(name)
            view.findNavController().navigate(direction)
        }

        fun bind(covidDataDashboards: CovidDataAndCovidDataDashboards) {
            with(binding) {
                viewModel = CovidDataAndCovidDataDashboardsViewModel(covidDataDashboards)
                executePendingBindings()
            }
        }
    }
}

private class CovidDataDashboardDiffCallback: DiffUtil.ItemCallback<CovidDataAndCovidDataDashboards>() {

    override fun areItemsTheSame(oldItem: CovidDataAndCovidDataDashboards, newItem: CovidDataAndCovidDataDashboards): Boolean =
        oldItem.covidData.name == newItem.covidData.name

    override fun areContentsTheSame(oldItem: CovidDataAndCovidDataDashboards, newItem: CovidDataAndCovidDataDashboards): Boolean =
        oldItem.covidData == newItem.covidData
}