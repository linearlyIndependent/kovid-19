/*
 * *
 *  * [Kovid-19] Kovid19PagerAdapter.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 6/28/20 12:02 AM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 6/28/20 12:02 AM.
 *
 */

package xyz.ploechl.kovid_19.adapter

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import xyz.ploechl.kovid_19.CovidDataDashboardFragment
import xyz.ploechl.kovid_19.CovidDataListFragment
import xyz.ploechl.kovid_19.utility.COVID_DATA_DASHBOARD_PAGE_INDEX
import xyz.ploechl.kovid_19.utility.COVID_DATA_LIST_PAGE_INDEX

class Kovid19PagerAdapter(fragment: Fragment): FragmentStateAdapter(fragment) {

    /**
     * Map [HomeViewPagerFragment] page indexes to the respective Fragments.
     */
    private val tabFragmentsCreators: Map<Int, () -> Fragment> = mapOf(
        COVID_DATA_DASHBOARD_PAGE_INDEX to { CovidDataDashboardFragment() },
        COVID_DATA_LIST_PAGE_INDEX to { CovidDataListFragment() }
    )

    override fun getItemCount(): Int = tabFragmentsCreators.size

    @Throws(IndexOutOfBoundsException::class)
    override fun createFragment(position: Int): Fragment =
        tabFragmentsCreators[position]?.invoke() ?: throw IndexOutOfBoundsException()

}