/*
 * *
 *  * [Kovid-19] CovidDataDashboardListItemAdapter.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 7/1/20 12:15 AM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 7/1/20 12:15 AM.
 *
 */

package xyz.ploechl.kovid_19.adapter

import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.google.android.material.floatingactionbutton.FloatingActionButton
import xyz.ploechl.kovid_19.utility.DATE_FORMATTER
import java.util.Date


@BindingAdapter("dtToText")
fun formatDate(textView: TextView, dt: Date?) {

    if (dt != null) {
        //view.hide()
        textView.text = DATE_FORMATTER.format(dt).toString()
    }
}
