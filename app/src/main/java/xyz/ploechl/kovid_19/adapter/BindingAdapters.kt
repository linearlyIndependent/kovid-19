/*
 * *
 *  * [Kovid-19] BindingAdapters.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 6/27/20 7:01 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 6/27/20 7:01 PM.
 *
 */

package xyz.ploechl.kovid_19.adapter

import android.view.View
import android.widget.TextView
import androidx.databinding.BindingAdapter

@BindingAdapter("isGone")
fun bindIsGone(view: View, isGone: Boolean) {
    if (isGone) {
        view.visibility = View.GONE
    } else {
        view.visibility = View.VISIBLE
    }
}

@BindingAdapter("intToText")
fun intToText(view: TextView, int: Int?) {
    if (int == null) {
        view.text = "<null_value>"
    } else {
        view.text = int.toString()
    }
}