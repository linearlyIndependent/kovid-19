/*
 * *
 *  * [Kovid-19] CovidDataDetailBindingAdapters.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 6/27/20 10:59 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 6/27/20 10:59 PM.
 *
 */

package xyz.ploechl.kovid_19.adapter

import android.util.Log
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.google.android.material.floatingactionbutton.FloatingActionButton
import xyz.ploechl.kovid_19.R
import java.text.DecimalFormat
import java.text.MessageFormat
import kotlin.math.floor

//private const val GLOBAL_PERCENTAGE_PATTERN = "{0}%"
private const val VALUE_PERCENTAGE_PATTERN = "{0} ({1}%)"
private val GLOBAL_PERCENTAGE_DECIMAL_PATTERN = DecimalFormat("#.##")

@BindingAdapter("isGone")
fun bindIsGone(view: FloatingActionButton, isGone: Boolean?) {
    if (isGone == null || isGone) {
        view.hide()
        //view.setBackgroundResource(R.drawable.ic_plus)
    } else {
        view.show()
        //view.setBackgroundResource(R.drawable.ic_remove)
    }
}

@BindingAdapter(value = ["progress", "worldProgress"], requireAll = true)
fun bindProgress(progressBar: ProgressBar, progress: Int, worldProgress: Int) {
    Log.d(::bindProgress.toString(), "${progress} / ${worldProgress}")

    if (progress == 0 || worldProgress == 0) {
        progressBar.progress = 0
    } else {
        var percentage = (progress.toDouble() / worldProgress.toDouble()) * 100.0

        Log.d(::bindProgress.toString(), percentage.toString())

        if (percentage < 1) {
            percentage = 1.0
        }

        progressBar.progress = floor(percentage).toInt()
    }

}

//TODO: make 1 from these!!!!
@BindingAdapter(value = ["progressForString", "worldProgressForString"], requireAll = true)
fun bindPercentageString(textView: TextView, progressForString: Int, worldProgressForString: Int) {
    textView.text = calculatePercentageString(progressForString, worldProgressForString)
}

/**
 * Generates a percentage string with the required default format.
 */
private fun calculatePercentageString(value: Int, total: Int): String {
    var percentage: Double = 0.0

    if (total != 0) {
        percentage = (value.toDouble() / total.toDouble()) * 100.0
    }

    return MessageFormat.format(VALUE_PERCENTAGE_PATTERN, value, GLOBAL_PERCENTAGE_DECIMAL_PATTERN.format(percentage))
}

/*
@BindingAdapter("active")
fun bindActiveText(textView: TextView, active: Int) {
    textView.text = active.toString()
}

@BindingAdapter("confirmed")
fun bindConfirmedText(textView: TextView, confirmed: Int) {
    textView.text = confirmed.toString()
}

@BindingAdapter("deaths")
fun bindDeathsText(textView: TextView, deaths: Int) {
    textView.text = deaths.toString()
}

@BindingAdapter("recovered")
fun bindRecoveredText(textView: TextView, recovered: Int) {
    textView.text = recovered.toString()
}
*/