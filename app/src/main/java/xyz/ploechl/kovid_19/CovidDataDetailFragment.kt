/*
 * *
 *  * [Kovid-19] CovidDataDetailFragment.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 6/28/20 5:11 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 6/28/20 5:11 PM.
 *
 */

package xyz.ploechl.kovid_19

import android.content.Intent
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.app.ShareCompat
import androidx.core.widget.NestedScrollView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.covid_data_detail_fragment.*
import xyz.ploechl.kovid_19.data.CovidData
import xyz.ploechl.kovid_19.databinding.CovidDataDetailFragmentBinding
import xyz.ploechl.kovid_19.utility.InjectorUtilities
import xyz.ploechl.kovid_19.utility.createCovidDataStatsString
import xyz.ploechl.kovid_19.viewmodel.CovidDataDetailViewModel
import java.text.DecimalFormat
import java.text.MessageFormat

class CovidDataDetailFragment : Fragment() {

    private val args: CovidDataDetailFragmentArgs by navArgs()

    private val covidDataDetailViewModel: CovidDataDetailViewModel by viewModels {
        InjectorUtilities.provideCovidDataDetailViewModelFactory(requireActivity(), this.args.covidDataId)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        var binding: CovidDataDetailFragmentBinding
        try {
            binding = DataBindingUtil.inflate<CovidDataDetailFragmentBinding>(
                inflater,
                R.layout.covid_data_detail_fragment,
                container,
                false
            )
                .apply {
                    viewModel = covidDataDetailViewModel
                    lifecycleOwner = viewLifecycleOwner
                    callback = object : Callback {

                        override fun addClicked(covidData: CovidData?) {
                            covidData?.let {
                                hideAppBarFab(fab)
                                showAppBarFab(fab_remove)
                                covidDataDetailViewModel.addCovidDataToCovidDataDashboard()
                                Snackbar.make(root, R.string.added_covid_data_dashboard, Snackbar.LENGTH_LONG).show()
                            }
                        }

                        override fun removeClicked(covidData: CovidData?) {
                            covidData?.let {
                                hideAppBarFab(fab_remove)
                                showAppBarFab(fab)
                                covidDataDetailViewModel.removeCovidDataFromCovidDataDashboard()
                                Snackbar.make(root, R.string.removed_covid_data_dashboard, Snackbar.LENGTH_LONG).show()
                            }
                        }
                    }

                    var isToolbarShown = false

                    covidDataDetailScrollview.setOnScrollChangeListener(
                        NestedScrollView.OnScrollChangeListener { _, _, scrollY, _, _ ->
                            val showToolbar = scrollY > toolbar.height

                            //Show toolbar and title on toolbar if it was scrolled past.
                            if (isToolbarShown != showToolbar) {
                                isToolbarShown = showToolbar

                                appbar.isActivated = showToolbar
                                toolbarLayout.isTitleEnabled = showToolbar
                            }
                        }
                    )

                    toolbar.setNavigationOnClickListener { view ->
                        view.findNavController().navigateUp()
                    }

                    toolbar.setOnMenuItemClickListener { item ->
                        when (item.itemId) {
                            R.id.action_share -> {
                                // Share data as text.
                                createShareIntent()
                                true
                            }
                            else -> false
                        }
                    }
                }
        } catch (e: Exception) {
            Log.e(::onCreateView.toString(), e.toString(), e)
            throw e
        }

        setHasOptionsMenu(true)

        return binding.root
        //return inflater.inflate(R.layout.covid_data_detail_fragment, container, false)
    }

    @Suppress("DEPRECATION")
    private fun createShareIntent() {
        val covidData = covidDataDetailViewModel.covidData.value
        val covidDataWorld = covidDataDetailViewModel.covidWorldData.value

        if (covidData == null || covidDataWorld == null) {
            Log.d(::createShareIntent.toString(), "Data is null cannot share this.")
            return
        }

        val resultString = StringBuilder(getString(R.string.covid_data_share_header)).append("\n\n")
            .append(createCovidDataStatsString(covidData, covidDataWorld, requireContext()))
            .toString()

        Log.d(::createShareIntent.toString(), resultString)

        //Fixed with
        // https://stackoverflow.com/questions/52835036/how-do-i-get-activity-instead-of-fragmentactivty
        startActivity(ShareCompat.IntentBuilder.from(requireActivity())
            .setType("text/plain")
            .setText(resultString)
            .createChooserIntent()
            .addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT or Intent.FLAG_ACTIVITY_MULTIPLE_TASK))
    }

    // FloatingActionButtons anchored to AppBarLayouts have their visibility controlled by the scroll position.
    // We want to turn this behavior off to hide the FAB when it is clicked.
    //
    // This is adapted from Chris Banes' Stack Overflow answer: https://stackoverflow.com/a/41442923
    private fun hideAppBarFab(fab: FloatingActionButton) {
        val params = fab.layoutParams as CoordinatorLayout.LayoutParams
        val behavior = params.behavior as FloatingActionButton.Behavior
        behavior.isAutoHideEnabled = false
        fab.hide()
    }

    // FloatingActionButtons anchored to AppBarLayouts have their visibility controlled by the scroll position.
    // We want to turn this behavior off to hide the FAB when it is clicked.
    //
    // This is adapted from Chris Banes' Stack Overflow answer: https://stackoverflow.com/a/41442923
    private fun showAppBarFab(fab: FloatingActionButton) {
        val params = fab.layoutParams as CoordinatorLayout.LayoutParams
        val behavior = params.behavior as FloatingActionButton.Behavior
        behavior.isAutoHideEnabled = false
        fab.show()
    }

    interface Callback {
        fun addClicked(covidData: CovidData?)
        fun removeClicked(covidData: CovidData?)
    }
}