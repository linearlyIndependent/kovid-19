/*
 * *
 *  * [Kovid-19] HomeViewPagerFragment.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 6/28/20 3:49 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 6/28/20 3:49 PM.
 *
 */

package xyz.ploechl.kovid_19

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.tabs.TabLayoutMediator
import xyz.ploechl.kovid_19.adapter.Kovid19PagerAdapter
import xyz.ploechl.kovid_19.databinding.FragmentViewPagerBinding
import xyz.ploechl.kovid_19.utility.COVID_DATA_DASHBOARD_PAGE_INDEX
import xyz.ploechl.kovid_19.utility.COVID_DATA_LIST_PAGE_INDEX

/**
 * A simple [Fragment] subclass.
 * Use the [HomeViewPagerFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeViewPagerFragment : Fragment() {
    /**
     * Gets the icon for each tab.
     */
    private fun getTabIcon(position: Int): Int =
        when (position) {
            COVID_DATA_LIST_PAGE_INDEX -> R.drawable.covid_list_tab_selector
            COVID_DATA_DASHBOARD_PAGE_INDEX -> R.drawable.dashboard_tab_selector
            else -> {
                val exc = IndexOutOfBoundsException("No icon for position ${position}!")
                Log.e(::getTabIcon.toString(), "No icon for position ${position}!", exc)
                throw exc
            }
        }

    /**
     * Gets the title for each tab.
     */
    private fun getTabTitle(position: Int): String? =
        when(position) {
            COVID_DATA_LIST_PAGE_INDEX -> getString(R.string.covid_list_title)
            COVID_DATA_DASHBOARD_PAGE_INDEX -> getString(R.string.dashboard_title)
            else -> {
                val exc = IndexOutOfBoundsException("No tab title for position ${position}!")
                Log.e(::getTabTitle.toString(), "No tab title for position ${position}!", exc)
                throw exc
            }
        }

    /**
     * Binds the Fragment to the View when it's created.
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentViewPagerBinding.inflate(inflater, container, false)
        val tabLayout = binding.tabs
        val viewPager2 = binding.viewPager

        // Hook up the adapter to the viewpager.
        viewPager2.adapter = Kovid19PagerAdapter(this)

        // Link together the tablayout and the viewpager, while also setting the title and icon.
        TabLayoutMediator(tabLayout, viewPager2) { tab, position ->
            tab.setIcon(getTabIcon(position))
            tab.text = getTabTitle(position)
        }.attach()

        // Sets the toolbar to act as an actionbar for this activity.
        (activity as AppCompatActivity).setSupportActionBar(binding.toolbar)

        return binding.root
    }
}