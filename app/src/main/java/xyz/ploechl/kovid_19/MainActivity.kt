/*
 * *
 *  * [Kovid-19] MainActivity.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 6/27/20 4:24 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 6/27/20 4:24 PM.
 *
 */

package xyz.ploechl.kovid_19

import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil.setContentView
import android.os.Bundle
import xyz.ploechl.kovid_19.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView<ActivityMainBinding>(this, R.layout.activity_main)
    }

}