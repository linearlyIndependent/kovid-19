/*
 * *
 *  * [Kovid-19] constants.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 6/26/20 11:26 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 6/26/20 11:26 PM.
 *
 */

package xyz.ploechl.kovid_19.utility

import java.text.SimpleDateFormat
import java.util.*


// Constants for CovidDatabase
const val DATABASE_NAME = "kovid_19-db"
const val DEFAULT_SETTINGS_FILENAME = "default_settings.json"


// Constants for JsonParser

/**
 * This level 0 key retrieves the data of the JSON.
 */
internal const val L0_DATA = "data"

/**
 * This level 0 key retrieves the timestamp when
 * the data retrieved in the JSON was last modified.
 */
internal const val L0_DT = "dt"

/**
 * This level 0 key retrieves the TS of the JSON.
 */
internal const val L0_TS = "ts"

/**
 * This and other level 1 keys retrieve data from the data array.
 *
 * L1_LOCATION holds a [String], the other L1 keys hold an [int].
 */
internal const val L1_LOCATION = "location"
internal const val L1_CONFIRMED = "confirmed"
internal const val L1_DEATHS = "deaths"
internal const val L1_RECOVERED = "recovered"
internal const val L1_ACTIVE = "active"


/**
 * The DateFormat used by the API.
 *
 * Implementation changed for API 21 compatibility.
 */
internal val DATE_FORMATTER = SimpleDateFormat("MM-dd-yyyy", Locale.US)
//DateTimeFormatter.ofPattern("MM-dd-yyyy")


// Constants for Kovid19PagerAdapter
internal const val COVID_DATA_DASHBOARD_PAGE_INDEX = 0
internal const val COVID_DATA_LIST_PAGE_INDEX = 1
