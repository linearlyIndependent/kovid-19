/*
 * *
 *  * [Kovid-19] viewUtils.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 7/7/20 7:08 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 7/7/20 7:07 PM.
 *
 */

package xyz.ploechl.kovid_19.utility

import android.content.Context
import android.util.Log
import xyz.ploechl.kovid_19.R
import xyz.ploechl.kovid_19.data.CovidData
import xyz.ploechl.kovid_19.data.CovidDataAndCovidDataDashboards
import java.text.DecimalFormat
import java.text.MessageFormat


internal val VALUE_PERCENTAGE_PATTERN = "{0} ({1}%)"
internal val GLOBAL_PERCENTAGE_DECIMAL_PATTERN = DecimalFormat("#.###")

internal fun createPercentageString(value: Int, total: Int): String {

    var percentage = 0.0

    if (total != 0) {
        percentage = (value.toDouble() / total.toDouble()) * 100.0
    }

    return MessageFormat.format(VALUE_PERCENTAGE_PATTERN, value, GLOBAL_PERCENTAGE_DECIMAL_PATTERN.format(percentage))
}

internal fun createCovidDataStatsString(covidDataList: List<CovidData>, covidDataWorld: CovidData, context: Context): String =
    // Again, I love Kotlin for all the functional stuff... <3
    covidDataList.joinToString(separator = "\n\n") { createCovidDataStatsString(it, covidDataWorld, context) }

@JvmName("createCovidDataStatsStringDataAndDashboard")
internal fun createCovidDataStatsString(covidDataList: List<CovidDataAndCovidDataDashboards>, covidDataWorld: CovidData, context: Context): String =
    // Again, I love Kotlin for all the functional stuff... <3
     createCovidDataStatsString(covidDataList.map { it.covidData }, covidDataWorld, context)


internal fun createCovidDataStatsString(covidData: CovidData, covidDataWorld: CovidData, context: Context): String {
    val newline = "\n\n"

    //context.getString(R.string.covid_data_share_header)
    val resultString = StringBuilder()
        .append(covidData.name).append(newline)
        .append("${context.getString(R.string.confirmed)}: ").append(
            createPercentageString(
                covidData.confirmed,
                covidDataWorld.confirmed
            )
        ).append(newline)
        .append("${context.getString(R.string.active)}: ").append(
            createPercentageString(
                covidData.active,
                covidDataWorld.active
            )
        ).append(newline)
        .append("${context.getString(R.string.recovered)}: ").append(
            createPercentageString(
                covidData.recovered,
                covidDataWorld.recovered
            )
        ).append(newline)
        .append("${context.getString(R.string.deaths)}: ").append(
            createPercentageString(
                covidData.deaths,
                covidDataWorld.deaths
            )
        ).append(newline)
        .append(context.getString(R.string.covid_data_share_date_text)).append(covidData.dt).append(newline)
        .toString()

    return resultString
}