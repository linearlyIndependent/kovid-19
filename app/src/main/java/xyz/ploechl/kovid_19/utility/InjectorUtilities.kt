/*
 * *
 *  * [Kovid-19] InjectorUtilities.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 6/27/20 6:10 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 6/27/20 6:10 PM.
 *
 */

package xyz.ploechl.kovid_19.utility

import android.content.Context
import androidx.fragment.app.Fragment
import xyz.ploechl.kovid_19.data.CovidDataDashboardRepository
import xyz.ploechl.kovid_19.data.CovidDataRepository
import xyz.ploechl.kovid_19.data.CovidDatabase
import xyz.ploechl.kovid_19.data.SettingRepository
import xyz.ploechl.kovid_19.viewmodel.CovidDataDashboardListViewModelFactory
import xyz.ploechl.kovid_19.viewmodel.CovidDataDetailViewModelFactory
import xyz.ploechl.kovid_19.viewmodel.CovidDataListViewModelFactory

/**
 * Static methods for all the repositories to inject them into Activities and Fragments.
 */
object InjectorUtilities {

    private fun getCovidDataRepository(context: Context): CovidDataRepository = 
        CovidDataRepository.getInstance(CovidDatabase.getInstance(context.applicationContext).covidDataDao())

    private fun getCovidDataDashboardRepository(context: Context): CovidDataDashboardRepository =
        CovidDataDashboardRepository.getInstance(CovidDatabase.getInstance(context.applicationContext).covidDataDashboardDao())

    private fun getSettingRepository(context: Context): SettingRepository =
        SettingRepository.getInstance(CovidDatabase.getInstance(context.applicationContext).settingDao())

    fun provideCovidDataListViewModelFactory(fragment: Fragment): CovidDataListViewModelFactory =
        CovidDataListViewModelFactory(getCovidDataRepository(fragment.requireContext()), getSettingRepository(fragment.requireContext()), fragment)

    fun provideCovidDataDetailViewModelFactory(context: Context, covidDataId: String): CovidDataDetailViewModelFactory =
        CovidDataDetailViewModelFactory(getCovidDataRepository(context), getCovidDataDashboardRepository(context), covidDataId)

    fun provideCovidDataDashboardListViewModelFactory(context: Context): CovidDataDashboardListViewModelFactory =
        CovidDataDashboardListViewModelFactory(getCovidDataDashboardRepository(context), getSettingRepository(context), getCovidDataRepository(context))
}