/*
 * *
 *  * [Kovid-19] CovidDataListFragment.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 6/28/20 4:54 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 6/28/20 4:54 PM.
 *
 */

package xyz.ploechl.kovid_19

import android.content.Intent
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.core.app.ShareCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import com.google.android.material.snackbar.Snackbar
import xyz.ploechl.kovid_19.adapter.CovidDataAdapter
import xyz.ploechl.kovid_19.data.Setting
import xyz.ploechl.kovid_19.databinding.FragmentCovidDataListBinding
import xyz.ploechl.kovid_19.utility.InjectorUtilities
import xyz.ploechl.kovid_19.utility.createCovidDataStatsString
import xyz.ploechl.kovid_19.viewmodel.CovidDataListViewModel


class  CovidDataListFragment: Fragment() {
    private val viewModel: CovidDataListViewModel by viewModels {
        InjectorUtilities.provideCovidDataListViewModelFactory(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentCovidDataListBinding.inflate(inflater, container, false)
        context ?: return binding.root

        val adapter = CovidDataAdapter()
        binding.covidDataList.adapter = adapter

        subscribeUi(adapter)

        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) =
        inflater.inflate(R.menu.menu_covid_list, menu)

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            // Update data menu item
            R.id.update_covid_data_item -> {
                //TODO: implement data refreshing function here!!
                this.viewModel.updateDataDatabase().invokeOnCompletion {
                    Log.d(::onOptionsItemSelected.toString(), "Updated dataset.")

                    Toast.makeText(
                        context,
                        getString(R.string.data_updated_toast),
                        Toast.LENGTH_LONG
                    ).show()
                }

                true
            }
            // Share data as text.
            R.id.action_share -> {
                this.createShareIntent()
                true
            }
            // Update ordering
            R.id.order_name_item -> {
                this.viewModel.updateSetting(Setting.Companion.OrderingValue.NAME)
                true
            }
            R.id.order_active_item -> {
                this.viewModel.updateSetting(Setting.Companion.OrderingValue.ACTIVE)
                true
            }
            R.id.order_confirmed_item -> {
                this.viewModel.updateSetting(Setting.Companion.OrderingValue.CONFIRMED)
                true
            }
            R.id.order_recovered_item -> {
                this.viewModel.updateSetting(Setting.Companion.OrderingValue.RECOVERED)
                true
            }
            R.id.order_deaths_item -> {
                this.viewModel.updateSetting(Setting.Companion.OrderingValue.DEATHS)
                true
            }
            // Update ascending/descending
            R.id.order_ad_ascending_item -> {
                this.viewModel.updateSetting(Setting.Companion.AscDescValue.ASCENDING)
                true
            }
            R.id.order_ad_descending_item -> {
                this.viewModel.updateSetting(Setting.Companion.AscDescValue.DESCENDING)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    private fun subscribeUi(adapter: CovidDataAdapter) =
        this.viewModel.covidDataEntries.observe(viewLifecycleOwner) { covidDataEntries ->
            adapter.submitList(
                covidDataEntries
            )
        }

    @Suppress("DEPRECATION")
    private fun createShareIntent() {
        val covidData = this.viewModel.covidDataEntries.value
        val covidDataWorld = this.viewModel.covidDataWorld.value

        if (covidData == null || covidDataWorld == null) {
            Log.d(::createShareIntent.toString(), "Data is null cannot share this.")
            return
        }


        val resultString = StringBuilder(getString(R.string.covid_data_share_header)).append("\n\n")
            .append(createCovidDataStatsString(covidData, covidDataWorld, requireContext()))
            .toString()

        Log.d(::createShareIntent.toString(), resultString)

        //Fixed with
        // https://stackoverflow.com/questions/52835036/how-do-i-get-activity-instead-of-fragmentactivty
        startActivity(
            ShareCompat.IntentBuilder.from(requireActivity())
            .setType("text/plain")
            .setText(resultString)
            .createChooserIntent()
            .addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT or Intent.FLAG_ACTIVITY_MULTIPLE_TASK))
    }

}
/*
    private fun updateDataOrdering() =
        with(viewModel) {

        }

class CovidDataListFragment : Fragment() {

    companion object {
        fun newInstance() = CovidDataListFragment()
    }

    private lateinit var viewModel: CovidDataListViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.covid_data_list_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(CovidDataListViewModel::class.java)
        // TODO: Use the ViewModel
    }

}
*/