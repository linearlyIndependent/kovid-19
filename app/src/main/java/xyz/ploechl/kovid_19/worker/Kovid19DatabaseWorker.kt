/*
 * *
 *  * [Kovid-19] Kovid19DatabaseWorker.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 6/26/20 11:34 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 6/26/20 11:34 PM.
 *
 */

package xyz.ploechl.kovid_19.worker

import android.content.Context
import android.util.Log
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import kotlinx.coroutines.coroutineScope
import xyz.ploechl.kovid_19.data.CovidData
import xyz.ploechl.kovid_19.data.CovidDatabase
import xyz.ploechl.kovid_19.network_io.ApiPath
import xyz.ploechl.kovid_19.network_io.buildUrl
import xyz.ploechl.kovid_19.network_io.getDataFromHttps
import xyz.ploechl.kovid_19.parser.parseCovidCountryDataFromJsonString
import xyz.ploechl.kovid_19.parser.parseCovidWorldDataFromJsonString
import java.lang.Exception

/**
 * Database worker which refreshes the database entries from the API.
 */
class Kovid19DatabaseWorker(context: Context, workerParams: WorkerParameters): CoroutineWorker(context, workerParams) {
    //TODO: check this later
    override suspend fun doWork(): Result = coroutineScope {
        try {
            //TODO: Add here the calls to network IO and json parsing!!!
            val covidDataList: List<CovidData> = fetchCountryAndWorldCovidData()!!

            val database = CovidDatabase.getInstance(applicationContext)
            database.covidDataDao().insertAll(covidDataList)

            Log.d(::doWork.toString(), "Success while setting up the database!")
            Log.d(::doWork.toString(), covidDataList.toString())
            Result.success()

        } catch (ex: Exception) {
            Log.e(::doWork.toString(), "Error while setting up the database!", ex)
            Result.failure()
        }
    }

    //TODO: here I may also update the db every time it opens, need to think about it...

    /**
     * Fetches the covid country and world data from the API, returns null in case of error.
     */
    private fun fetchCountryAndWorldCovidData(): List<CovidData>? {
        // I know I should use an if-else because it's better for performance but I enjoy the functional parts fo Kotlin so much...
        val countryDataString: String = getDataFromHttps(buildUrl(ApiPath.CURRENT)) ?: run {
            Log.e(::fetchCountryAndWorldCovidData.toString(), "Could not fetch country dataset, got null string result.")
            return null
        }

        Log.d(::fetchCountryAndWorldCovidData.toString(), countryDataString)

        val worldDataString: String = getDataFromHttps(buildUrl(ApiPath.TOTAL)) ?: run {
            Log.e(::fetchCountryAndWorldCovidData.toString(), "Could not fetch world data entry, got null string result.")
            return null
        }

        Log.d(::fetchCountryAndWorldCovidData.toString(), worldDataString)

        val countryDataList: List<CovidData> = parseCovidCountryDataFromJsonString(countryDataString) ?: run {
            Log.e(::fetchCountryAndWorldCovidData.toString(), "Could not parse covid country JSON string, got null list result.")
            return null
        }

        Log.d(::fetchCountryAndWorldCovidData.toString(), countryDataList.toString())

        val worldCovidData: CovidData = parseCovidWorldDataFromJsonString(worldDataString) ?: run {
            Log.e(::fetchCountryAndWorldCovidData.toString(), "Could not parse covid world JSON string, got null entry result.")
            return null
        }

        Log.d(::fetchCountryAndWorldCovidData.toString(), worldCovidData.toString())

        val mutableCovidList = countryDataList.toMutableList()
        mutableCovidList.add(0, worldCovidData)
        return mutableCovidList
    }
}