/*
 * *
 *  * [Kovid-19] SettingDatabaseWorker.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 7/5/20 10:52 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 7/5/20 10:52 PM.
 *
 */

package xyz.ploechl.kovid_19.worker

import android.content.Context
import android.util.Log
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import kotlinx.coroutines.coroutineScope
import xyz.ploechl.kovid_19.data.CovidDatabase
import xyz.ploechl.kovid_19.file_io.readSettingsFromJson

class SettingDatabaseWorker(context: Context, workerParams: WorkerParameters): CoroutineWorker(context, workerParams) {
    override suspend fun doWork(): Result = coroutineScope {
        try {
            val settingsList = readSettingsFromJson(applicationContext.assets)!!
            val database = CovidDatabase.getInstance(applicationContext)
            database.settingDao().insertSettingsList(settingsList)

            Log.d(::doWork.toString(), "Success while setting up the settings table!")
            Log.d(::doWork.toString(), settingsList.toString())

            Result.success()
        } catch (ex: Exception) {
            Log.e(::doWork.toString(), "Error while setting up database!", ex)
            Result.failure()
        }
    }
}