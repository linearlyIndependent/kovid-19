/*
 * *
 *  * [Kovid-19] FileIO.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 7/6/20 9:55 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 7/6/20 9:55 PM.
 *
 */

package xyz.ploechl.kovid_19.file_io

import android.content.res.AssetManager
import android.util.JsonReader
import android.util.Log
import org.json.JSONException
import xyz.ploechl.kovid_19.data.Setting
import xyz.ploechl.kovid_19.utility.DEFAULT_SETTINGS_FILENAME
import java.nio.file.AccessMode


/**
 * Parses a JSON string into JSON objects and retrieves the default [Setting]s..
 */
internal fun readSettingsFromJson(assetManager: AssetManager): List<Setting>? {
    try {
        val settingsList: MutableList<Setting> = emptyList<Setting>().toMutableList()

        assetManager.open(DEFAULT_SETTINGS_FILENAME, AccessMode.READ.ordinal).use { inputStream ->
            JsonReader(inputStream.reader()).use { jsonReader ->

                jsonReader.beginArray()

                while (jsonReader.hasNext()) {
                    var key: String? = null
                    var value: String? = null

                    jsonReader.beginObject()

                    while (jsonReader.hasNext()) {
                        val name: String = jsonReader.nextName()

                        when (name) {
                            "key" -> key = jsonReader.nextString()
                            "value" -> value = jsonReader.nextString()
                            else -> Log.w(::readSettingsFromJson.toString(), "Unexpected entry in JSON.")
                        }
                    }

                    jsonReader.endObject()

                    if (key == null || value == null) {
                        Log.w(::readSettingsFromJson.toString(), "Key and/or value was null.")
                        continue
                    }

                    val setting: Setting

                    when(key) {
                        Setting.ASC_DESC_KEY -> setting = Setting(key, value)
                        Setting.ORDERING_KEY -> setting = Setting(key, value)
                        else -> throw JSONException("Invalid key value pair in settings JSON.")
                    }

                    settingsList.add(setting)

                    Log.d(::readSettingsFromJson.toString(), "Parsed setting: $setting")
                }
            }
        }

        return settingsList

    } catch (ex: Exception) {
        Log.e(::readSettingsFromJson.toString(), "Error seeding database", ex)
        return null
    }
}