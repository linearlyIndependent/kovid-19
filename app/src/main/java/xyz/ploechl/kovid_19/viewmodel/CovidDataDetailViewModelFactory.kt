/*
 * *
 *  * [Kovid-19] CovidDataDetailViewModelFactory.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 6/28/20 5:16 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 6/28/20 5:16 PM.
 *
 */

package xyz.ploechl.kovid_19.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import xyz.ploechl.kovid_19.data.CovidDataDashboardRepository
import xyz.ploechl.kovid_19.data.CovidDataRepository

/**
 * A factory class that creates [CovidDataDetailViewModel]s with a constructor accepting a [CovidDataRepository] and a [CovidData] ID.
 */
class CovidDataDetailViewModelFactory(
    private val covidDataRepository: CovidDataRepository,
    private val covidDataDashboardRepository: CovidDataDashboardRepository,
    private val covidDataId: String
): ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
        CovidDataDetailViewModel(covidDataRepository, covidDataDashboardRepository, covidDataId) as T
}