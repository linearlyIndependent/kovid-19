/*
 * *
 *  * [Kovid-19] CovidDataAndCovidDataDashboardsViewModel.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 6/28/20 12:38 AM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 6/28/20 12:38 AM.
 *
 */

package xyz.ploechl.kovid_19.viewmodel

import xyz.ploechl.kovid_19.data.CovidDataAndCovidDataDashboards
import xyz.ploechl.kovid_19.utility.DATE_FORMATTER
import java.text.SimpleDateFormat
import java.util.*

class CovidDataAndCovidDataDashboardsViewModel(covidDataDashboards: CovidDataAndCovidDataDashboards) {
    private val covidData = checkNotNull(covidDataDashboards.covidData)
    private val covidDataDashboard = covidDataDashboards.covidDataDashboards[0]

    val covidDataDt: Date
        get() = this.covidData.dt
    val covidDataName: String
        get() = this.covidData.name
    val covidDataActive: Int
        get() = this.covidData.active
    val covidDataConfirmed: Int
        get() = this.covidData.confirmed
    val covidDataDeaths: Int
        get() = this.covidData.deaths
    val covidDataRecovered: Int
        get() = this.covidData.recovered
}