/*
 * *
 *  * [Kovid-19] CovidDataDetailViewModel.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 6/28/20 5:14 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 6/28/20 5:12 PM.
 *
 */

package xyz.ploechl.kovid_19.viewmodel

import android.util.Log
import androidx.annotation.Nullable
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.*
import kotlinx.coroutines.launch
import xyz.ploechl.kovid_19.data.*

/**
 * The [ViewModel] used in the [CovidDataDetailViewFragment].
 */
class CovidDataDetailViewModel(
    covidDataRepository: CovidDataRepository,
    private val covidDataDashboardRepository: CovidDataDashboardRepository,
    private val covidDataId: String
): ViewModel() {
    val isOnDashboard = this.covidDataDashboardRepository.isCovidDataOnDashboard(this.covidDataId)
    val covidData = covidDataRepository.getCovidData(this.covidDataId)
    val covidWorldData = covidDataRepository.getCovidData(CovidData.WORLD_ID)

    
    /**
     * Adds a [CovidData] to a [CovidDataDashboard] and saves it.
     */
    fun addCovidDataToCovidDataDashboard() {
        viewModelScope.launch {
            covidDataDashboardRepository.createCovidDataDashboard(covidDataId)
        }
    }

    /**
     * Removes a [CovidData] together with its [CovidDataDashboard] if possible.
     */
    fun removeCovidDataFromCovidDataDashboard() =
        viewModelScope.launch {
            covidDataDashboardRepository.deleteCovidDataDashboards(covidDataId)
        }
            /*
            val dashboards = covidDataDashboardRepository.getDashboardsOfCovidDataSync(covidDataId)
            dashboards.observe(, Observer<List<CovidDataAndCovidDataDashboards>>() {
                @Override
                fun onChanged(dashboardsList: List<CovidDataAndCovidDataDashboards>? ) {
                    if (dashboardsList != null) {
                        viewModelScope.launch {
                            dashboardsList.forEach {
                                it.covidDataDashboards.forEach {
                                    Log.d(".removeCovidDataToCo...", "Removing: ${it}")
                                }
                            }

                            mediatorDashboards.removeSource(dashboards)
                        }
                    } else {
                        Log.d(".removeCovidDataToCo...", "Could not remove, no dashboard found: ${covidDataId} --> ${dashboardsList}")
                        mediatorDashboards.removeSource(dashboards)
                    }
                }
            })

                */


            /*
            val dashboards = covidDataDashboardRepository.getCreatedCovidDataDashboards()
            //val dashboardsWithData = covidDataDashboardRepository.getDashboardOfCovidData(covidDataId)
            //val dashboardList = dashboardsWithData.value

            //TODO: check this later if it's okay.
            if (dashboards.value != null) {
                val dashboardsList = dashboards.value
                val dashboardOfData:CovidDataDashboard? = dashboardsList?.filter{ it.covidData.name == covidDataId}?.flatMap { it.covidDataDashboards }?.firstOrNull()

                if (dashboardOfData != null) {
                    Log.d(".removeCovidDataToCo...", "Dashboard found, removal possible: ${covidDataId} --> ${dashboardOfData}")
                    covidDataDashboardRepository.removeCovidDataDashboard(dashboardOfData)
                } else {
                    Log.d(".removeCovidDataToCo...", "Could not remove, no dashboard found: ${covidDataId} --> ${dashboardOfData}")
                }
            } else {
                Log.e(".removeCovidDataToCo...", "No results or multiple results in CovidDataDetailViewModel.removeCovidDataToCovidDataDashboard although there should be one.")
            }
*/
}

