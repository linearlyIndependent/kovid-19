/*
 * *
 *  * [Kovid-19] CovidDataDashboardListViewModel.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 7/2/20 10:41 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 7/2/20 10:41 PM.
 *
 */

package xyz.ploechl.kovid_19.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import xyz.ploechl.kovid_19.data.*

class CovidDataDashboardListViewModel constructor(
    private val covidDataDashboardRepository: CovidDataDashboardRepository,
    private val settingRepository: SettingRepository,
    private val covidDataRepository: CovidDataRepository
): ViewModel() {
    val settings = settingRepository.getSettings()
    val covidDataAndCovidDataDashboards: LiveData<List<CovidDataAndCovidDataDashboards>> = Transformations.switchMap(this.settings) { settingsList -> this.getEntriesWithOrdering(settingsList) }
    val covidDataWorld = this.covidDataRepository.getCovidData(CovidData.WORLD_ID)

    //val covidDataAndCovidDataDashboards: LiveData<List<CovidDataAndCovidDataDashboards>> =
    //    covidDataDashboardRepository.getCreatedCovidDataDashboards()

    /**
     * Updates the data in the database.
     */
    fun updateDataDatabase() = viewModelScope.launch {
        covidDataDashboardRepository.updateCovidDataFromRemote()
    }


    /**
     * Updates the specified setting entry in the database.
     */
    fun updateSetting(orderingValue: Setting.Companion.OrderingValue) {
        viewModelScope.launch {
            settingRepository.saveSettingEntry(Setting.ORDERING_KEY, orderingValue.name)
        }
    }

    /**
     * Updates the specified setting entry in the database.
     */
    fun updateSetting(ascDescValue: Setting.Companion.AscDescValue) {
        viewModelScope.launch {
            settingRepository.saveSettingEntry(Setting.ASC_DESC_KEY, ascDescValue.name)
        }
    }

    /**
     * Returns a reordered list of all covid data dashboards and covid datas in them when the ordering changes.
     */
    private fun getEntriesWithOrdering(settingsList: List<Setting>): LiveData<List<CovidDataAndCovidDataDashboards>> {
        var orderingValue: Setting.Companion.OrderingValue? = null
        var ascDescValue: Setting.Companion.AscDescValue? = null

        for (setting in settingsList) {

            try {
                when(setting.key) {
                    Setting.ORDERING_KEY -> orderingValue = Setting.Companion.OrderingValue.valueOf(setting.value)
                    Setting.ASC_DESC_KEY -> ascDescValue = Setting.Companion.AscDescValue.valueOf(setting.value)
                    else -> Log.w(::getEntriesWithOrdering.toString(), "Encountered invalid setting key: ${setting.key}")
                }
            } catch (ex: Exception) {
                Log.e(::getEntriesWithOrdering.toString(), "Encountered invalid setting value: ${setting.key}: ${setting.value}", ex)
            }
        }

        if (orderingValue == null) {
            orderingValue = Setting.Companion.OrderingValue.NAME
        }

        if (ascDescValue == null) {
            ascDescValue = Setting.Companion.AscDescValue.ASCENDING
        }

        return this.covidDataDashboardRepository.getCreatedCovidDataDashboards(orderingValue, ascDescValue)
    }
}
