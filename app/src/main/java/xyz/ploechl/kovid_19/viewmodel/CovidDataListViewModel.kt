/*
 * *
 *  * [Kovid-19] CovidDataListViewModel.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 6/28/20 4:56 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 6/28/20 4:56 PM.
 *
 */

package xyz.ploechl.kovid_19.viewmodel

import android.util.Log
import androidx.lifecycle.*
import kotlinx.coroutines.launch
import xyz.ploechl.kovid_19.data.CovidData
import xyz.ploechl.kovid_19.data.CovidDataRepository
import xyz.ploechl.kovid_19.data.Setting
import xyz.ploechl.kovid_19.data.SettingRepository

/**
 * The [ViewModel] used in the [CovidDataListFragment].
 */
class CovidDataListViewModel  constructor(
    private val covidDataRepository: CovidDataRepository, //TODO: if this crashes it might be because of private val
    private val settingRepository: SettingRepository,
    private val savedStateHandle: SavedStateHandle
): ViewModel() {
    val settings = settingRepository.getSettings() //TODO: get setting here, then map on these with switchmap like in the comment (on both!) then the list will update automatically, just modify the function to take setting instead and take the other setting
    val covidDataEntries = Transformations.switchMap(this.settings) { settingsList -> this.getEntriesWithOrdering(settingsList) }
    val covidDataWorld = this.covidDataRepository.getCovidData(CovidData.WORLD_ID)

    //TODO: delete these and their methods they are obsolete
    //val orderingSetting = Transformations.map(this.settings) { settingsList -> this.getOrdering(settingsList) }
    //val ascDescSetting = Transformations.map(this.settings) { settingsList -> this.getAscDesc(settingsList) }
 /*
    fun setSavedOrdering(ordering: String) =
        this.savedStateHandle.set(COVID_DATA_ORDERING_SAVED_STATE_KEY, ordering)

    fun getSavedOrdering(): MutableLiveData<String> =
        this.savedStateHandle.getLiveData(COVID_DATA_ORDERING_SAVED_STATE_KEY, DEFAULT_ORDERING)

    fun hasOrdering(ordering: String): Boolean =
        this.getSavedOrdering().value == ordering
*/

    /**
     * Updates the data in the database.
     */
    fun updateDataDatabase() = viewModelScope.launch {
        covidDataRepository.updateCovidDataFromRemote()
    }

    /**
     * Returns a reordered list of all covid data entries when the ordering changes
     */
    private fun getEntriesWithOrdering(settingsList: List<Setting>): LiveData<List<CovidData>> {
        var orderingValue: Setting.Companion.OrderingValue? = null
        var ascDescValue: Setting.Companion.AscDescValue? = null

        for (setting in settingsList) {

            try {
                when(setting.key) {
                    Setting.ORDERING_KEY -> orderingValue = Setting.Companion.OrderingValue.valueOf(setting.value)
                    Setting.ASC_DESC_KEY -> ascDescValue = Setting.Companion.AscDescValue.valueOf(setting.value)
                    else -> Log.w(::getEntriesWithOrdering.toString(), "Encountered invalid setting key: ${setting.key}")
                }
            } catch (ex: Exception) {
                Log.e(::getEntriesWithOrdering.toString(), "Encountered invalid setting value: ${setting.key}: ${setting.value}", ex)
            }
        }

        if (orderingValue == null) {
            orderingValue = Setting.Companion.OrderingValue.NAME
        }

        if (ascDescValue == null) {
            ascDescValue = Setting.Companion.AscDescValue.ASCENDING
        }

        return this.covidDataRepository.getCovidDataEntries(orderingValue, ascDescValue)
    }

    /**
     * Updates the specified setting entry in the database.
     */
    fun updateSetting(orderingValue: Setting.Companion.OrderingValue) {
        viewModelScope.launch {
            settingRepository.saveSettingEntry(Setting.ORDERING_KEY, orderingValue.name)
        }
    }

    /**
     * Updates the specified setting entry in the database.
     */
    fun updateSetting(ascDescValue: Setting.Companion.AscDescValue) {
        viewModelScope.launch {
            settingRepository.saveSettingEntry(Setting.ASC_DESC_KEY, ascDescValue.name)
        }
    }


    //TODO: delete these and their methods they are obsolete
/*
    /**
     * Parses the [Setting.Companion.OrderingValue] from the settings list.
     */
    private fun getOrdering(settingsList: List<Setting>): Setting.Companion.OrderingValue {
        val orderingSetting = settingsList.firstOrNull { it.key == Setting.ORDERING_KEY }

        if (orderingSetting != null) {
            try {
                val orderingValue = Setting.Companion.OrderingValue.valueOf(orderingSetting.value)
                Log.d(::getOrdering.toString(), "Parsed ordering: ${orderingValue}")

                return orderingValue

            } catch (ex: Exception) {
                Log.e(::getEntriesWithOrdering.toString(), "Encountered invalid ordering value: ${orderingSetting.value}", ex)
            }

        } else {
            Log.w(::getOrdering.toString(), "Settings do not contain an ordering entry, fallback to default: ${Setting.Companion.OrderingValue.NAME}")
        }

        return Setting.Companion.OrderingValue.NAME
    }

    /**
     * Parses the [Setting.Companion.AscDescValue] from the settings list.
     */
    private fun getAscDesc(settingsList: List<Setting>): Setting.Companion.AscDescValue {
        val ascDescSetting = settingsList.firstOrNull { it.key == Setting.ASC_DESC_KEY }

        if (ascDescSetting != null) {
            try {
                val orderingValue = Setting.Companion.AscDescValue.valueOf(ascDescSetting.value)
                Log.d(::getOrdering.toString(), "Parsed asc-desc value: ${ascDescSetting}")

                return orderingValue

            } catch (ex: Exception) {
                Log.e(::getEntriesWithOrdering.toString(), "Encountered invalid asc-desc value: ${ascDescSetting.value}", ex)
            }

        } else {
            Log.w(::getOrdering.toString(), "Settings do not contain an asc-desc entry, fallback to default: ${Setting.Companion.AscDescValue.ASCENDING}")
        }

        return Setting.Companion.AscDescValue.ASCENDING
    }
*/
}