/*
 * *
 *  * [Kovid-19] CovidDataDashboardListViewModelFactory.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 7/2/20 10:45 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 7/2/20 10:45 PM.
 *
 */

package xyz.ploechl.kovid_19.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import xyz.ploechl.kovid_19.data.CovidDataDashboardRepository
import xyz.ploechl.kovid_19.data.CovidDataRepository
import xyz.ploechl.kovid_19.data.SettingRepository

/**
 * A factory class that creates [CovidDataDashboardListViewModel]s with a constructor accepting a [CovidDataDashboardRepository] and a [SettingRepository].
 */
class CovidDataDashboardListViewModelFactory(
    private val repository: CovidDataDashboardRepository,
    private val settingRepository: SettingRepository,
    private val covidDataRepository: CovidDataRepository
): ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
        CovidDataDashboardListViewModel(repository, settingRepository, covidDataRepository) as T

}