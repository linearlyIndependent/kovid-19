/*
 * *
 *  * [Kovid-19] CovidDataListViewModelFactory.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 6/28/20 5:02 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 6/28/20 5:02 PM.
 *
 */

package xyz.ploechl.kovid_19.viewmodel

import android.os.Bundle
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistryOwner
import xyz.ploechl.kovid_19.data.CovidDataRepository
import xyz.ploechl.kovid_19.data.SettingRepository

/**
 * A factory class that creates [CovidDataListViewModelFactory]s with a custom constructor.
 */
class CovidDataListViewModelFactory(
    private val repository: CovidDataRepository,
    private val settingsRepository: SettingRepository,
    owner: SavedStateRegistryOwner,
    defaultArgs: Bundle? = null
): AbstractSavedStateViewModelFactory(owner, defaultArgs) {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(key: String, modelClass: Class<T>, handle: SavedStateHandle): T =
        CovidDataListViewModel(repository, settingsRepository, handle) as T
}