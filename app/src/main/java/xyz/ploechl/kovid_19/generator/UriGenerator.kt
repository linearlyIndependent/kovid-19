/*
 * *
 *  * [Kovid-19] UriGenerator.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 5/17/20 11:14 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 5/17/20 11:14 PM.
 *
 */

package xyz.ploechl.kovid_19.generator

import android.net.Uri

/**
 * Generates a Geo-Uri based on the given [latitude], [longitude] and [location].
 */
internal fun generateGeoURI(latitude: Double, longitude: Double, location: String): Uri {
    return Uri.Builder()
        .scheme("geo")
        .encodedOpaquePart("$latitude,$longitude")
        .appendQueryParameter("q", location).build()
}

/**
 * Generates a Geo-Uri based on the given [location].
 */
internal fun generateGeoURI(location: String): Uri {
    return generateGeoURI(latitude= 0.0, longitude = 0.0, location = location)
}

/**
 * Generates a Geo-Uri based on the given [latitude] and [longitude].
 */
internal fun generateGeoURI(latitude: Double, longitude: Double): Uri {
    return Uri.Builder()
        .scheme("geo")
        .encodedOpaquePart("$latitude,$longitude").build()
}