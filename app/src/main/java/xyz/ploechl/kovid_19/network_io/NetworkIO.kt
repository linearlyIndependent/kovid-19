/*
 * *
 *  * [Kovid-19] NetworkIO.kt
 *  * Created by Patrik Ploechl (p@ploechl.xyz) on 5/17/20 11:29 PM.
 *  * Copyright (c) 2020. All rights reserved.
 *  * Last modified 5/17/20 11:29 PM.
 *
 */

package xyz.ploechl.kovid_19.network_io

import android.net.Uri
import android.util.Log
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.net.MalformedURLException
import java.net.URL
import javax.net.ssl.HttpsURLConnection

/**
 * The base URI.
 */
private val BASE_URI = Uri.parse("https://covid2019-api.herokuapp.com/v2")

/**
 * Represents different resource paths that can be appended to the base URL in [xyz.ploechl.kovid_19.network_io].
 *
 * @see  <a href="https://github.com/nat236919/Covid2019API">COVID2019-API (V2)</a>
 */
internal enum class ApiPath(val path: String) {
    // Current all countries separate.
    CURRENT("current"),  // Current global.
    TOTAL("total"),
    CONFIRMED("confirmed"),
    DEATHS("deaths"),
    RECOVERED("recovered"),
    ACTIVE("active"),
    COUNTRY("country"),
    TIMESERIES("timeseries");
}

/**
 * Builds the URL for fetching the COVID-19 datasets.
 *
 * @param path the suffix to be added to the base URI to specify the resource
 * @param base_URI the base URI
 * @param parameter an optional query parameter
 * @return the URL
 */
internal fun buildUrl(path: ApiPath, base_URI: Uri = BASE_URI, parameter: String? = null): URL? {
    val uriBuilder = base_URI.buildUpon().appendPath(path.path)

    if (parameter != null) uriBuilder.appendPath(parameter)

    try {
        val url = URL(uriBuilder.build().toString())
        Log.d(::buildUrl.toString(), url.toString())
        return url
    } catch (e: UnsupportedOperationException) {
        Log.e(::buildUrl.toString(), "Error making COVID-19 fetching URL.", e)
    } catch (e: MalformedURLException) {
        Log.e(::buildUrl.toString(), "Error making COVID-19 fetching URL.", e)
    }

    return null
}

/**
 * Returns the response string for a dataset fetched over HTTPS.
 *
 * Style based on my C# best-practice memories (using clause)
 * and some research how to do them in Java,
 * which was now converted to Kotlin using the [use] function.
 *
 * Inspiration:
 * @see https://www.infoq.com/news/2010/08/arm-blocks/
 * @see https://alvinalexander.com/blog/post/java/simple-https-example/
 */
internal fun getDataFromHttps(nullableUrl: URL?): String? {
    val url = nullableUrl ?: return null

    var connection: HttpsURLConnection
    try {
           connection = url.openConnection() as HttpsURLConnection
        } catch (e: IOException) {
        Log.e(::getDataFromHttps.toString(), "Error establishing HTTPS collection.", e)
        return null
    }

    connection.requestMethod = "GET"

    var result = ""
    try {
        connection.inputStream.use { inputStream ->
            InputStreamReader(inputStream).use { inputStreamReader ->
                BufferedReader(inputStreamReader).use {bufferedReader ->
                    result = bufferedReader.readLine()
                }
            }
        }
    } catch (e: Exception) {
        Log.e(::getDataFromHttps.toString(), "Error fetching data through HTTPS.", e)
        return null
    }

    // Log.d(::getDataFromHttps.toString(), result)

    return result
}