A Kotlin/Android application created for the Advanced Android App Development Credit Course.

The application fetches data from the [COVID2019-API](https://github.com/nat236919/Covid2019API) API and is a rewritten (from Java to Kotlin) and enhanced version of [Covid-19](https://gitlab.com/linearlyIndependent/covid-19).
